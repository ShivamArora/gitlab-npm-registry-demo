# Gitlab NPM Registry Demo

This repository provides a demo for publishing npm packages to Gitlab NPM Registry using different package managers including npm, yarn and lerna.

Each folder represents a new node project created using the respective package manager and contains the required setup and configuration for publishing npm packages to Gitlab NPM Registry.

> **Note:** The `.npmrc` in each folder represents the npm configuration for the respective project. It is suggested to keep the config at the project root unless you've different needs.

You can also find a detailed guide for this setup by reading this blog on [Publishing your private npm packages to Gitlab NPM Registry](https://medium.com/@shivamarora/publishing-your-private-npm-packages-to-gitlab-npm-registry-39d30a791085)

## Get a hands-on experience

To get a hands-on for publishing packages, you can:

1. Fork this repository

2. Replace `@my-org` with your org name or username in the respective project of the repo or in all projects.

3. Replace `<your-project-id>` with your own project id in the respective project of the repo or in all projects.

4. Create a deploy token for authentication. 

   The steps to generate a deploy token has been discussed in [Publishing your private npm packages to Gitlab NPM Registry](https://medium.com/@shivamarora/publishing-your-private-npm-packages-to-gitlab-npm-registry-39d30a791085) Give the article a read.

5. Finally, publish the package using the package manager of your choice.

## Published Packages

You can also checkout some of the published packages in the [Packages & Registries](https://gitlab.com/ShivamArora/gitlab-npm-registry-demo/-/packages) section on the left navigation bar.